package net.pl3x.pl3xlibs;

import net.pl3x.pl3xlibs.Pl3xLibs;
import PluginReference.MC_World;

public class World {
	private MC_World rainbowWorld;

	public World(MC_World world) {
		this.rainbowWorld = world;
	}

	public World(String name) {
		this.rainbowWorld = Pl3xLibs.getWorld(name);
	}

	public World(Integer dimension) {
		this.rainbowWorld = Pl3xLibs.getWorld(dimension);
	}

	public String getName() {
		return Pl3xLibs.getWorldName(rainbowWorld);
	}

	public Block getBlockAt(int x, int y, int z) {
		return getChunkAt(x >> 4, z >> 4).getBlock(x & 0xF, y & 0xFF, z & 0xF);
	}

	public Block getBlockAt(Location loc) {
		return getBlockAt(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ());
	}

	public Chunk getChunkAt(int x, int z) {
		return new Chunk(this, x, z);
	}

	public Chunk getChunkAt(Block block) {
		return getChunkAt(block.getX() >> 4, block.getZ() >> 4);
	}

	public Chunk getChunkAt(Location location) {
		return getChunkAt(location.getBlockX() >> 4, location.getBlockZ() >> 4);
	}

	public boolean equals(World world) {
		return getName().equals(world.getName());
	}

	public MC_World getHandle() {
		return rainbowWorld;
	}
}
