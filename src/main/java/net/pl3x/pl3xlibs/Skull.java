package net.pl3x.pl3xlibs;

public enum Skull {
	PLAYER("player", 3, ""),
	BAT("bat", 3, "ManBatPlaysMC"),
	BLAZE("blaze", 3, "MHF_Blaze"),
	CAVE_SPIDER("cave_spider", 3, "MHF_CaveSpider"),
	CHICKEN("chicken", 3, "MHF_Chicken"),
	COW("cow", 3, "MHF_Cow"),
	CREEPER("creeper", 4, ""),
	ENDERDRAGON("enderdragon", 3, "KingEndermen"),
	ENDERMAN("enderman", 3, "MHF_Enderman"),
	ENDERMITE("endermite", 3, "MinecadeHead"),
	GHAST("ghast", 3, "MHF_Ghast"),
	GIANT("giant", 2, ""),
	GUARDIAN("guardian", 3, "Creepypig7"),
	HORSE("horse", 3, "Horse_Fucker"),
	LAVA_SLIME("lava_slime", 3, "MHF_LavaSlime"),
	MUSHROOM_COW("mushroom_cow", 3, "MHF_MushroomCow"),
	OCELOT("ocelot", 3, "MHF_Ocelot"),
	PIG("pig", 3, "MHF_Pig"),
	PIG_ZOMBIE("pig_zombie", 3, "MHF_PigZombie"),
	RABBIT("rabbit", 3, "rabbit2077"),
	SHEEP("sheep", 3, "MHF_Sheep"),
	SILVERFISH("silverfish", 3, "minecade2"),
	SKELETON("skeleton", 0, ""),
	SLIME("slime", 3, "MHF_Slime"),
	SNOWMAN("snowman", 3, "snowmandk"),
	SPIDER("spider", 3, "MHF_Spider"),
	SQUID("squid", 3, "MHF_Squid"),
	VILLAGER("villager", 3, "MHF_Villager"),
	VILLAGER_GOLEM("villager_golem", 3, "MHF_Golem"),
	WITCH("witch", 3, "scrafbrothers4"),
	WITHERBOSS("witherboss", 3, "MHF_Wither"),
	WITHERSKELETON("withersketeton", 1, ""),
	WOLF("wolf", 3, "joshua278"),
	ZOMBIE("zombie", 2, "");

	private String name;
	private int type;
	private String skin;

	Skull(String name, int type, String skin) {
		this.name = name;
		this.type = type;
		this.skin = skin;
	}

	public String getName() {
		return name;
	}

	public int getType() {
		return type;
	}

	public String getSkin() {
		return skin;
	}

	public Skull setSkin(String skin) {
		this.skin = skin;
		return this;
	}
}
