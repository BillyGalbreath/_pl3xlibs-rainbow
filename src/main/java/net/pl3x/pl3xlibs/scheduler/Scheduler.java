package net.pl3x.pl3xlibs.scheduler;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Scheduler {
	private HashMap<String, Set<Task>> tasks = new HashMap<String, Set<Task>>();

	protected Task addTask(String pluginName, Task task) {
		Set<Task> pluginTasks = new HashSet<Task>();
		if (tasks.containsKey(pluginName)) {
			pluginTasks = tasks.get(pluginName);
			if (pluginTasks.contains(task)) {
				return null;
			}
		}
		pluginTasks.add(task);
		tasks.put(pluginName, pluginTasks);
		return task;
	}

	protected void removeTask(String pluginName, Task task) {
		Set<Task> pluginTasks = new HashSet<Task>();
		if (tasks.containsKey(pluginName)) {
			pluginTasks = tasks.get(pluginName);
			if (!pluginTasks.contains(task)) {
				return;
			}
			pluginTasks.remove(task);
		}
		if (pluginTasks.isEmpty()) {
			tasks.remove(pluginName);
		} else {
			tasks.put(pluginName, pluginTasks);
		}
	}

	public void cancelAllRunnables(String pluginName) {
		if (!tasks.containsKey(pluginName)) {
			return;
		}
		for (Task task : tasks.get(pluginName)) {
			task.cancel();
		}
		tasks.remove(pluginName);
	}

	public Task scheduleTask(String pluginName, Pl3xRunnable runnable, int delay) {
		Task newTask = new Task(runnable, delay);
		return addTask(pluginName, newTask);
	}

	public Task scheduleRepeatingTask(String pluginName, Pl3xRunnable runnable, int delay, int period) {
		Task newTask = new Task(runnable, delay, period);
		return addTask(pluginName, newTask);
	}

	public Task scheduleAsynchronousTask(String pluginName, Pl3xRunnable runnable, int delay) {
		AsynchronousTask newTask = new AsynchronousTask(runnable, delay);
		return addTask(pluginName, newTask);
	}

	public Task scheduleAsynchronousRepeatingTask(String pluginName, Pl3xRunnable runnable, int delay, int period) {
		AsynchronousTask newTask = new AsynchronousTask(runnable, delay, period);
		return addTask(pluginName, newTask);
	}

	public void tick() {
		Set<Task> toRemove = new HashSet<Task>();
		for (Map.Entry<String, Set<Task>> entry : new HashMap<String, Set<Task>>(tasks).entrySet()) {
			String pluginName = entry.getKey();
			Set<Task> pluginTasks = entry.getValue();
			for (Task task : new HashSet<Task>(pluginTasks)) {
				task.doTick();
				if (task.isCancelled()) {
					toRemove.add(task);
				}
			}
			for (Task task : toRemove) {
				pluginTasks.remove(task);
			}
			tasks.put(pluginName, pluginTasks);
		}
	}
}
