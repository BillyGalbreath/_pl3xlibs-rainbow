package net.pl3x.pl3xlibs.scheduler;

public class Task {
	private Pl3xRunnable runnable;
	private Integer delay = -1;
	private Integer period = -1;
	private boolean cancelled = false;

	public Task(Pl3xRunnable runnable, Integer delay) {
		this(runnable, delay, -1);
	}

	public Task(Pl3xRunnable runnable, Integer delay, Integer period) {
		this.runnable = runnable;
		this.delay = delay;
		this.period = period;
		runnable.task = this;
	}

	public void run() {
		if (this instanceof AsynchronousTask) {
			(new Thread() {
				public void run() {
					runnable.run();
				}
			}).start();
		} else {
			runnable.run();
		}
	}

	public void cancel() {
		delay = -1;
		period = -1;
		cancelled = true;
	}

	public boolean isCancelled() {
		return cancelled;
	}

	protected void doTick() {
		if (cancelled) {
			return; // scheduled for cancellation
		}
		if (delay < 0) {
			cancel();
			return; // task expired, cancel
		}
		if (delay > 0) {
			delay--;
			return; // delay before first starting
		}
		run();
		delay = period;
	}
}
