package net.pl3x.pl3xlibs.configuration;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import net.pl3x.pl3xlibs.BetterPluginInfo;
import net.pl3x.pl3xlibs.Pl3xLibs;
import PluginReference.MC_Location;
import PluginReference.PluginInfo;
import _Pl3xLibs.MyPlugin;

/**
 * Base Config for INI file format
 * 
 * @author BillyGalbreath
 */
public class BaseConfig {
	protected File configFile;
	protected ConfigProperties properties;
	protected Class<?> clazz;
	protected BetterPluginInfo pluginInfo;
	protected String file;
	protected String path;
	protected HashMap<String, String> map = new HashMap<String, String>();
	protected Set<String> removed = new HashSet<String>();

	public BaseConfig(Class<?> clazz, PluginInfo pluginInfo, String path, String file) {
		this.pluginInfo = new BetterPluginInfo(pluginInfo);
		this.file = file;
		this.path = path;
		this.clazz = clazz;
		configFile = new File(this.pluginInfo.getPluginDirectory() + path + file);
	}

	public void load() {
		load(true);
	}

	public void load(boolean useDefaultFromJar) {
		if (!configFile.exists()) {
			configFile.getParentFile().mkdirs();
			if (useDefaultFromJar) {
				copyFileFromJarToDisk(path + file, false);
			}
		}
		if (!configFile.exists()) {
			// create new empty file if still does not exist
			try {
				configFile.createNewFile();
			} catch (IOException e) {
				MyPlugin.getInstance().getLogger().error("Failed to create configuration file: " + configFile.getAbsoluteFile());
			}
		}
		reload();
	}

	public void reload() {
		map.clear();
		properties = new ConfigProperties();
		try {
			FileReader fileReader = new FileReader(configFile);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
			properties.load(bufferedReader);
			bufferedReader.close();
			fileReader.close();
		} catch (Exception e) {
			MyPlugin.getInstance().getLogger().error("Failed to load configuration file: " + configFile.getAbsoluteFile());
			e.printStackTrace();
		}
		for (Object key : properties.keySet()) {
			if (!(key instanceof String)) {
				continue;
			}
			map.put((String) key, properties.getProperty((String) key));
		}
	}

	public void save() {
		try {
			properties = new ConfigProperties();
			FileReader reader = new FileReader(configFile);
			BufferedReader bufferedReader = new BufferedReader(reader);
			properties.load(bufferedReader);
			bufferedReader.close();
			reader.close();
			for (String key : removed) {
				if (!(key instanceof String)) {
					continue;
				}
				properties.remove(key);
			}
			for (String key : map.keySet()) {
				if (!(key instanceof String)) {
					continue;
				}
				properties.put(key, map.get(key));
			}
			FileWriter fileWriter = new FileWriter(configFile);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			properties.store(bufferedWriter, null);
			bufferedWriter.close();
			fileWriter.close();
		} catch (Exception e) {
			MyPlugin.getInstance().getLogger().error("Failed to save configuration file: " + configFile.getAbsoluteFile());
			e.printStackTrace();
		}
	}

	private void copyFileFromJarToDisk(String fileName, boolean overwrite) {
		File file = new File(pluginInfo.getPluginDirectory(), fileName);
		if (!overwrite && file.exists()) {
			return; // file exists already, do NOT overwrite!
		}
		InputStream stream = clazz.getResourceAsStream("/" + fileName);
		if (stream == null) {
			return; // file not in jar
		}
		OutputStream resStreamOut = null;
		int readBytes;
		byte[] buffer = new byte[4096];
		try {
			resStreamOut = new FileOutputStream(file);
			while ((readBytes = stream.read(buffer)) > 0) {
				resStreamOut.write(buffer, 0, readBytes);
			}
			stream.close();
			resStreamOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getFilePath() {
		try {
			return configFile.getCanonicalPath();
		} catch (IOException ignore) {
		}
		return null;
	}

	public String get(String key) {
		return get(key, null);
	}

	public String get(String key, String def) {
		String value = map.get(key);
		return value == null ? def : value;
	}

	public void set(String key, String value) {
		map.put(key, value);
	}

	public void remove(String key) {
		if (!map.containsKey(key)) {
			return;
		}
		map.remove(key);
		removed.add(key);
	}

	public Integer getInteger(String key) {
		return getInteger(key, null);
	}

	public Integer getInteger(String key, Integer def) {
		String raw = get(key);
		if (raw == null) {
			return def;
		}
		Integer theInt;
		try {
			theInt = Integer.valueOf(raw);
		} catch (NumberFormatException e) {
			return def; // not a number!
		}
		return theInt;
	}

	public Double getDouble(String key) {
		return getDouble(key, null);
	}

	public Double getDouble(String key, Double def) {
		String raw = get(key);
		if (raw == null) {
			return def;
		}
		Double theDouble;
		try {
			theDouble = Double.valueOf(raw);
		} catch (NumberFormatException e) {
			return def; // not a number!
		}
		return theDouble;
	}

	public Long getLong(String key) {
		return getLong(key, null);
	}

	public Long getLong(String key, Long def) {
		String raw = get(key);
		if (raw == null) {
			return def;
		}
		Long theLong;
		try {
			theLong = Long.valueOf(raw);
		} catch (NumberFormatException e) {
			return def; // not a number!
		}
		return theLong;
	}

	public boolean getBoolean(String key) {
		return getBoolean(key, false);
	}

	public boolean getBoolean(String key, boolean def) {
		String raw = get(key);
		if (raw == null) {
			return def;
		}
		return raw.equalsIgnoreCase("true");
	}

	public List<String> getStringList(String key) {
		return getStringList(key, ",");
	}

	public List<String> getStringList(String key, String separator) {
		String raw = get(key);
		if (raw == null) {
			return null;
		}
		return Arrays.asList(raw.split(separator));
	}

	public MC_Location getLocation(String key) {
		return getLocation(key, null);
	}

	public MC_Location getLocation(String key, MC_Location def) {
		String raw = get(key);
		if (raw == null) {
			return def;
		}
		return Pl3xLibs.stringToLocation(raw);
	}

	public void setLocation(String key, MC_Location location) {
		set(key, Pl3xLibs.locationToString(location));
	}

	public HashMap<String, String> getMap(String key) {
		String raw = get(key);
		HashMap<String, String> map = new HashMap<String, String>();
		if (raw == null) {
			return map;
		}
		for (String tool : raw.split(";;")) {
			String[] split = tool.split("::");
			if (split.length != 2) {
				continue;
			}
			map.put(split[0], split[1]);
		}
		return map;
	}

	public void setMap(String key, HashMap<String, String> map) {
		String raw = "";
		for (Iterator<Map.Entry<String, String>> iter = map.entrySet().iterator(); iter.hasNext();) {
			Map.Entry<String, String> entry = iter.next();
			if (raw != null & !raw.equals("")) {
				raw += ";;";
			}
			raw += entry.getKey() + "::" + entry.getValue();
		}
		set(key, raw);
	}
}
