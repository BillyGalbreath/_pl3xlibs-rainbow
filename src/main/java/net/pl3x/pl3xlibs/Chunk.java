package net.pl3x.pl3xlibs;

public class Chunk {
	private World world;
	private int x;
	private int z;

	@SuppressWarnings("unused")
	private Chunk() {
	}

	/**
	 * Represents a chunk in the world
	 * 
	 * @param world World where chunk resides in
	 * @param x X location of chunk (chunk coordinates)
	 * @param z Z location of chunk (chunk coordinates)
	 */
	public Chunk(World world, int x, int z) {
		this.x = x;
		this.z = z;
		this.world = world;
	}

	/**
	 * Represents a chunk in the world
	 * 
	 * @param loc World location in chunk
	 */
	public Chunk(Location loc) {
		this(loc.getWorld(), loc.getBlockX() >> 4, loc.getBlockZ() >> 4);
	}

	/**
	 * Get X location (chunk coordinates)
	 * 
	 * @return X location
	 */
	public int getX() {
		return x;
	}

	/**
	 * Get Z location (chunk coordinates)
	 * 
	 * @return Z location
	 */
	public int getZ() {
		return z;
	}

	/**
	 * Get the world this chunk resides in
	 * 
	 * @return The world
	 */
	public World getWorld() {
		return world;
	}

	/**
	 * Get the block at specific location in relative to chunk
	 * 
	 * @param x Relative X location
	 * @param y Relative Y location
	 * @param z Relative Z location
	 * @return Block at location
	 */
	public Block getBlock(int x, int y, int z) {
		return new Block(this, (getX() << 4) | (x & 0xF), y & 0xFF, (getZ() << 4) | (z & 0xF));
	}
}
