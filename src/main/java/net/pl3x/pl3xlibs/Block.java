package net.pl3x.pl3xlibs;

import net.pl3x.pl3xlibs.Material;
import PluginReference.MC_Block;

public class Block {
	private Chunk chunk;
	private int x;
	private int y;
	private int z;

	/**
	 * Represents a block in the world
	 * 
	 * @param chunk Chunk where block resides
	 * @param x X location in the world
	 * @param y Y location in the world
	 * @param z Z location in the world
	 */
	public Block(Chunk chunk, int x, int y, int z) {
		this.x = x;
		this.y = y;
		this.z = z;
		this.chunk = chunk;
	}

	/**
	 * Get the world block resides in
	 * 
	 * @return The world
	 */
	public World getWorld() {
		return chunk.getWorld();
	}

	/**
	 * Get the chunk block resides in
	 * 
	 * @return The chunk
	 */
	public Chunk getChunk() {
		return chunk;
	}

	/**
	 * Get the X location
	 * 
	 * @return X location
	 */
	public int getX() {
		return x;
	}

	/**
	 * Get the Y location
	 * 
	 * @return Y location
	 */
	public int getY() {
		return y;
	}

	/**
	 * Get the Z location
	 * 
	 * @return Z location
	 */
	public int getZ() {
		return z;
	}

	/**
	 * Get the block type
	 * 
	 * @return Material of block
	 */
	public Material getType() {
		return Material.matchMaterial(Integer.toString(getTypeId()));
	}

	/**
	 * Get the block ID
	 * 
	 * @return ID of block
	 */
	public int getTypeId() {
		return getHandle().getId();
	}

	/**
	 * Get the block location
	 * 
	 * @return Location of block
	 */
	public Location getLocation() {
		return new Location(chunk.getWorld(), x, y, z);
	}

	/**
	 * Get the MC_Block for this block
	 * 
	 * @return MC_Block
	 */
	public MC_Block getHandle() {
		return chunk.getWorld().getHandle().getBlockAt(x, y, z);
	}
}
