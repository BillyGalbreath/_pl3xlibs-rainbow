package _Pl3xLibs;

import java.io.File;

import net.pl3x.pl3xlibs.Logger;
import net.pl3x.pl3xlibs.configuration.BaseConfig;
import net.pl3x.pl3xlibs.runnables.StartMetrics;
import net.pl3x.pl3xlibs.scheduler.Scheduler;

import org.fusesource.jansi.AnsiConsole;

import PluginReference.MC_Server;
import PluginReference.PluginBase;
import PluginReference.PluginInfo;

public class MyPlugin extends PluginBase {
	private static MyPlugin instance;
	private MC_Server server;
	private BaseConfig config;
	private BaseConfig dataConfig;
	private Scheduler scheduler;

	public PluginInfo getPluginInfo() {
		PluginInfo info = new PluginInfo();
		info.name = "Pl3xLibs";
		info.version = "0.3-SNAPSHOT";
		info.description = "Common utilities and libraries for RainbowMod plugins";
		info.optionalData.put("plugin-directory", "plugins_mod" + File.separator + info.name + File.separator);
		return info;
	}

	public static MyPlugin getInstance() {
		return instance;
	}

	@Override
	public void onStartup(MC_Server argServer) {
		server = argServer;
		instance = this;
		init();
		getLogger().info(getPluginInfo().name + " v" + getPluginInfo().version + " by BillyGalbreath is now enabled.");
	}

	@Override
	public void onShutdown() {
		disable();
		getLogger().info("Plugin disabled.");
	}

	public void init() {
		AnsiConsole.systemInstall();
		getConfig();
		getScheduler().scheduleTask(getPluginInfo().name, new StartMetrics(this), 100);
	}

	public void disable() {
		getScheduler().cancelAllRunnables(getPluginInfo().name);
		config = null;
		scheduler = null;
	}

	public void reload() {
		disable();
		init();
	}

	public MC_Server getServer() {
		return server;
	}

	public BaseConfig getConfig() {
		if (config == null) {
			config = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "config.ini");
			config.load();
		}
		return config;
	}

	public BaseConfig getDataConfig() {
		if (dataConfig == null) {
			dataConfig = new BaseConfig(MyPlugin.class, getPluginInfo(), "", "datastore.ini");
			dataConfig.load(false);
		}
		return dataConfig;
	}

	public Scheduler getScheduler() {
		if (scheduler == null) {
			scheduler = new Scheduler();
		}
		return scheduler;
	}

	public Logger getLogger(PluginInfo pluginInfo) {
		return new Logger(pluginInfo);
	}

	public Logger getLogger() {
		return getLogger(getPluginInfo());
	}

	@Override
	public void onTick(int tickNumber) {
		getScheduler().tick();
	}
}
